package com.example.android.mytimer;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import static com.example.android.mytimer.R.id.myTime;

public class MainActivity extends AppCompatActivity {

    TextView myText;
    Button myButt;
    Boolean myBool = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myText = (TextView)findViewById(myTime);

        myButt = (Button)findViewById(R.id.button);
        myButt.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){

                CountDownTimer time = new CountDownTimer(90000, 1000) {

                    public void onTick(long millisUntilFinished) {

                        myBool = true;

                        long myMill = millisUntilFinished;
                        String myStr = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(myMill), TimeUnit.MILLISECONDS.toSeconds(myMill) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(myMill)));



                        //myText.setText("seconds remaining: " + millisUntilFinished / 100000 + ":" + millisUntilFinished / 1000 );
                        myText.setText(myStr);
                        //here you can have your logic to set text to edittext
                    }

                    public void onFinish() {
                        myText.setText("done!");
                    }

                }.start();

            }


        });


    }
}
